#!/bin/bash

$git_token=$1
$CI_PROJECT_ID=$2
$CI_PIPELINE_ID=$3
$JOBNAME=$4
$JOBID=$5
$CI_PROJECT_URL=$6

echo -e "Trigger Gitlab Database PROD Deploy Job $CI_PROJECT_URL/-/jobs/$JOBID \n"
echo -e "curl --request POST --header "PRIVATE-TOKEN:${git_token} https://123.com/api/v4/projects/$CI_PROJECT_ID/jobs/$JOBID/play"
echo -e "\n \nTriggered Database PROD Deploy Job($JOBID).. "
